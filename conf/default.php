<?php
/**
 * Default settings for the json plugin
 *
 * @author Janez Paternoster <janez.paternoster@siol.net>
 */

$conf['ignore_if_no_plugin'] = 0;
$conf['null_str'] = '(null)';
$conf['array_str'] = '(array)';
$conf['true_str'] = '✔';
$conf['false_str'] = '☐';
$conf['src_recursive'] = 30;
$conf['json_display'] = 'inline,log';
$conf['enable_ejs'] = 0;
